const digits_only = string => [...string].every(c => '0123456789'.includes(c));

const getSum = (str1, str2) => {
  if(typeof str1 !== 'string' || typeof str2 !== 'string')
    return false;
  if(!digits_only(str1) || !digits_only(str2))
    return false;
  if(str1 === "")
    str1 = '0';
  if(str2 === "")
    str2 = '0';
  return (parseInt(str1) + parseInt(str2)).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var posts = 0, comments = 0;
  for(element of listOfPosts)
  {
    if(element.author == authorName)
    {
      posts++;
    }
    if(element.hasOwnProperty('comments'))
    {
      for(com of element.comments)
      {
        if(com.author == authorName)
          comments++;
      }
    }
  }
  return "Post:"+posts+",comments:"+comments;
};

const tickets=(people)=> {
  if(!Array.isArray(people) || people.length == 0)
    return "NO";
  var vasyaMoney = [0,0,0];
  for(let i = 0; i < people.length; i++)
  {
    var tmp = people[i];
    if(typeof tmp === 'string' || typeof tmp === 'symbol')
    {
      tmp = parseInt(tmp);
    }
    if(tmp == 25)
    {
      vasyaMoney[0]++;
      continue;
    }
    if(tmp == 50 && vasyaMoney[0] >= 1)
    {
      vasyaMoney[1]++;
      vasyaMoney[0]--;
      continue;
    }
    if(tmp == 100 && (vasyaMoney[0] >= 1 && vasyaMoney[1] >=1))
    {
      vasyaMoney[2]++;
      vasyaMoney[1]--;
      vasyaMoney[0]--;
      continue;
    }  
    if(tmp == 100 && vasyaMoney[0] >= 3)
    {
      vasyaMoney[2]++;
      vasyaMoney[0]-=3;
      continue;
    }
    return "NO";
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
